<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class() extends Migration
{
    public function up(): void
    {
        Schema::create('organizations', static function (Blueprint $table): void {
            $table->id();
            $table->string('inn');
            $table->jsonb('data');

            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('organizations');
    }
};
