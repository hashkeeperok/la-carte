<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class() extends Migration
{
    public function up(): void
    {
        Schema::create('organization_person', static function (Blueprint $table): void {
            $table->id();
            $table->string('organization_id');
            $table->string('person_id');
            $table->tinyInteger('type');
            $table->unsignedBigInteger('sync_id');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('organization_person');
    }
};
