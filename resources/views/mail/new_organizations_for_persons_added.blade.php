<!DOCTYPE html>
<html>
<head>
    <title>Новые организации для Физ Лиц</title>
</head>
<body>
<style>
    th {
        border: 1px black solid;
    }
</style>
<table>
    <thead>
    <tr>
        <th scope="col">ФИО</th>
        <th scope="col">ИНН</th>
        <th scope="col">Организации, в которых данное физлицо является руководителем</th>
        <th scope="col">Организации, в которых данное физлицо является учредителем</th>
        <th scope="col">Индивидуальные предприниматели</th>
    </tr>
    </thead>
    <tbody>
    <?php /** @var \App\Dto\Sync\PersonDto $person */ ?>
    @foreach ($newPersonOrganizationsDto->persons as $person)
        <tr>
            <th scope="row">{{ $person->lastName }} {{ $person->lastName }} {{ $person->middleName }}</th>
            <td>{{ $person->inn }}</td>
            <td>
                @foreach (array_filter(
                    $person->organizations,
                    fn (\App\Dto\Sync\OrganizationDto $organization) => $organization->type === \App\Enums\OrganizationTypeEnum::Head
                    ) as $organization)
                    {{ $organization->inn }}<br>
                @endforeach
            </td>
            <td>
                @foreach (array_filter(
                    $person->organizations,
                    fn (\App\Dto\Sync\OrganizationDto $organization) => $organization->type === \App\Enums\OrganizationTypeEnum::Founder
                    ) as $organization)
                    {{ $organization->inn }}<br>
                @endforeach
            </td>
            <td>
                @foreach (array_filter(
                    $person->organizations,
                    fn (\App\Dto\Sync\OrganizationDto $organization) => $organization->type === \App\Enums\OrganizationTypeEnum::Individual
                    ) as $organization)
                    {{ $organization->inn }}<br>
                @endforeach
            </td>
        </tr>
    @endforeach
</table>
</body>
</html>
