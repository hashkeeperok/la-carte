import axios from 'axios'

export default {
    namespaced: true,
    state: {
        authenticated: false,
        user: {}
    },
    getters: {
        authenticated(state) {
            return state.authenticated
        },
        user(state) {
            return state.user
        }
    },
    actions: {
        get(context, personId) {
            return axios.get(`/api/persons/${personId}`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
        },
        getAll(context) {
            return axios.get('/api/persons', {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
        },
        delete(context, personId) {
            return axios.delete(`/api/persons/${personId}`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
        },
        save(context, data) {
             return axios.post('/api/persons', data, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
        },
        update(context, {id, data}) {
            console.log(data)
            return axios.put(`/api/persons/${id}`, data, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
        },
        getSyncs(context) {
            return axios.get('/api/persons/syncs', {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
        },
        getSync(context, syncId) {
            return axios.get(`/api/persons/syncs/${syncId}`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
        },
        newSync(context) {
            return axios.post('/api/persons/syncs', {}, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
        },
    }
}
