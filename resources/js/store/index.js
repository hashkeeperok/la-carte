import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import auth from '@/store/auth'
import person from '@/store/person'

const store = createStore({
    plugins:[
        createPersistedState()
    ],
    modules:{
        auth,
        person
    }
})

export default store
