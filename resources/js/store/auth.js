import axios from 'axios'
import router from '@/router'

export default {
    namespaced: true,
    state: {
        authenticated: false,
        user: {}
    },
    getters: {
        authenticated(state) {
            return state.authenticated
        },
        user(state) {
            return state.user
        }
    },
    mutations: {
        SET_AUTHENTICATED(state, value) {
            state.authenticated = value
        },
        SET_USER(state, value) {
            state.user = value
        }
    },
    actions: {
        login(context, token) {
            localStorage.setItem("token", token);
            return axios.get('/api/user', {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }).then(({data}) => {
                context.commit('SET_USER', data)
                context.commit('SET_AUTHENTICATED', true)
                router.push({name: 'dashboard'})
            }).catch(({response: {data}}) => {
                context.commit('SET_USER', {})
                context.commit('SET_AUTHENTICATED', false)
                localStorage.removeItem("token");
            })
        },
        logout({commit}) {
            commit('SET_USER', {})
            commit('SET_AUTHENTICATED', false)
            localStorage.removeItem("token");
        }
    }
}
