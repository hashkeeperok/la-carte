import { createWebHistory, createRouter } from 'vue-router'
import store from '@/store'

/* Guest Component */
const Login = () => import('@/components/Login.vue')
const Register = () => import('@/components/Register.vue')
/* Guest Component */

/* Layouts */
const DashboardLayout = () => import('@/components/layouts/Default.vue')
/* Layouts */

/* Authenticated Component */
const Dashboard = () => import('@/components/Dashboard.vue')
/* Authenticated Component */

const NewPerson = () => import('@/components/NewPerson.vue')
const Syncs = () => import('@/components/Syncs.vue')
const Sync = () => import('@/components/Sync.vue')
const Person = () => import('@/components/Person.vue')
const Persons = () => import('@/components/Persons.vue')

const routes = [
    {
        name: "login",
        path: "/login",
        component: Login,
        meta: {
            middleware: "guest",
            title: `Login`
        }
    },
    {
        name: "register",
        path: "/register",
        component: Register,
        meta: {
            middleware: "guest",
            title: `Register`
        }
    },
    {
        path: "/",
        component: DashboardLayout,
        meta: {
            middleware: "auth"
        },
        children: [
            {
                name: "dashboard",
                path: '/',
                component: Dashboard,
                meta: {
                    title: `Dashboard`
                }
            },
            {
                name: "new_person",
                path: '/new_person',
                component: NewPerson,
                meta: {
                    title: `New Person`
                }
            },
            {
                name: "sync",
                path: '/syncs/:id',
                component: Sync,
                meta: {
                    title: `Sync`
                }
            },
            {
                name: "person",
                path: '/persons/:id',
                component: Person,
                meta: {
                    title: `Person`
                }
            },
            {
                name: "persons",
                path: '/persons',
                component: Persons,
                meta: {
                    title: `Persons`
                }
            },
            {
                name: "syncs",
                path: '/syncs',
                component: Syncs,
                meta: {
                    title: `Syncs`
                }
            }
        ]
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes, // short for `routes: routes`
})

router.beforeEach((to, from, next) => {
    document.title = to.meta.title
    if (to.meta.middleware == "guest") {
        if (store.state.auth.authenticated) {
            next({ name: "dashboard" })
        }
        next()
    } else {
        if (store.state.auth.authenticated) {
            next()
        } else {
            next({ name: "login" })
        }
    }
})

export default router
