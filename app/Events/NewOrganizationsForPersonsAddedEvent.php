<?php

declare(strict_types=1);

namespace App\Events;

use App\Dto\Sync\NewPersonOrganizationsDto;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewOrganizationsForPersonsAddedEvent
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public function __construct(public NewPersonOrganizationsDto $newPersonOrganizations) {}
}
