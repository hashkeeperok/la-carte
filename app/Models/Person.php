<?php

namespace App\Models;

use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property-read int                                  $id
 * @property      string                               $first_name
 * @property      string                               $last_name
 * @property      string                               $middle_name
 * @property      string                               $inn
 * @property      int                                  $language_id
 * @property      string                               $date_time_format
 * @property      string                               $time_zone
 * @property      CarbonImmutable                      $created_at
 * @property      CarbonImmutable                      $updated_at
 */
class Person extends Model
{
    use HasApiTokens, Notifiable;

    protected $table = 'persons';

    protected $guarded = [
        'id',
    ];

    public function organizations(): BelongsToMany
    {
        return $this->belongsToMany(Organization::class)->withPivot([
            'type',
        ]);
    }
}
