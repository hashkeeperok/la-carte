<?php

namespace App\Models;

use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property-read int $id
 * @property      string $inn
 * @property      array $data
 * @property      CarbonImmutable $created_at
 * @property      CarbonImmutable $updated_at
 */
class Organization extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $guarded = [
        'id',
    ];

    protected $casts = [
        'data' => 'array',
    ];
}
