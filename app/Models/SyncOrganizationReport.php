<?php

namespace App\Models;

use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property-read int $id
 * @property      string $path
 */
class SyncOrganizationReport extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $guarded = [
        'id',
    ];
}
