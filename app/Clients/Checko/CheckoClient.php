<?php

declare(strict_types=1);

namespace App\Clients\Checko;

use App\Clients\Checko\Dto\CheckoOrganization;
use App\Clients\Checko\Dto\CheckoPerson;
use App\Enums\OrganizationTypeEnum;
use App\Exceptions\Client\ClientDataException;
use App\Exceptions\Client\ClientRequestException;
use App\Exceptions\Client\ClientResponseCodeException;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class CheckoClient
{
    public function __construct(
        private Client $client,
        private string $host,
        private string $apiKey,
    ) {
    }

    /**
     * @throws ClientRequestException
     * @throws ClientResponseCodeException
     * @throws ClientDataException
     */
    public function getPerson(string $inn): CheckoPerson
    {
        $result = $this->sendRequest('GET', 'person', [
            'inn' => $inn,
        ]);

        $personData = $result['data'];

        try {
            $organizations = array_merge(
                $this->mapToOrganizations($personData['Руковод'], OrganizationTypeEnum::Head),
                $this->mapToOrganizations($personData['Учред'], OrganizationTypeEnum::Founder),
                $this->mapToOrganizations($personData['ИП'], OrganizationTypeEnum::Individual),
            );

            return new CheckoPerson(
                $personData['ИНН'],
                $personData['ФИО'],
                $organizations,
            );
        } catch (Throwable $throwable) {
            throw new ClientDataException('GET', 'person', $throwable);
        }
    }

    /**
     * @return array<int, CheckoOrganization>
     */
    private function mapToOrganizations(array $organizationsData, OrganizationTypeEnum $type): array
    {
        $result = [];

        foreach ($organizationsData as $organization) {
            $result[] = new CheckoOrganization(
                $organization['ИНН'],
                $type,
                $organizationsData,
            );
        }

        return $result;
    }

    /**
     * @throws ClientResponseCodeException
     * @throws ClientDataException
     * @throws ClientRequestException
     */
    private function sendRequest(string $method, string $uri, array $data = []): array
    {
        $queryString = http_build_query(['key' => $this->apiKey] + $data);

        $fullUrl = rtrim($this->host, '/').'/'.trim($uri, '/').'?'.$queryString;

        try {
            $response = $this->client->request($method, $fullUrl);
        } catch (GuzzleException $exception) {
            throw new ClientRequestException($method, $fullUrl, $exception);
        }

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new ClientResponseCodeException($method, $fullUrl, $response->getStatusCode());
        }

        try {
            return json_decode($response->getBody()->getContents(), true, 512, JSON_UNESCAPED_UNICODE | JSON_THROW_ON_ERROR);
        } catch (Exception $exception) {
            throw new ClientDataException($method, $fullUrl, $exception);
        }
    }
}
