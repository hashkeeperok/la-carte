<?php

declare(strict_types=1);

namespace App\Clients\Checko\Dto;

readonly class CheckoPerson
{
    /**
     * @param array<int, CheckoOrganization> $organizations
     */
    public function __construct(
        public string $inn,
        public string $fio,
        public array $organizations,
    ) {
    }
}
