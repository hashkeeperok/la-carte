<?php

declare(strict_types=1);

namespace App\Clients\Checko\Dto;

use App\Enums\OrganizationTypeEnum;

readonly class CheckoOrganization
{
    public function __construct(
        public string $inn,
        public OrganizationTypeEnum $type,
        public array $data,
    ) {
    }
}
