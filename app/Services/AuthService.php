<?php


declare(strict_types=1);

namespace App\Services;

use App\Dto\UserTokenDto;
use App\Enums\UserTokenEnum;
use App\Exceptions\Response\ValidationResponseException;
use App\Models\User;
use App\Repositories\UserRepository;
use Exception;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\PersonalAccessToken;

class AuthService
{
    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    /**
     * @throws Exception
     */
    public function login(string $email, string $password): UserTokenDto
    {
        $user = $this->userRepository->findByEmail($email);

        if ($user === null || !Hash::check($password, $user->password)) {
            throw new Exception('Пользователь не найден!'); // todo AuthException
        }

        return new UserTokenDto($email, $user->createToken('access-token')->plainTextToken);
    }

    public function logout(User $user)
    {
        $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
    }
}
