<?php

declare(strict_types=1);

namespace App\Services;

use App\Clients\Checko\CheckoClient;
use App\Clients\Checko\Dto\CheckoOrganization;
use App\Clients\Checko\Dto\CheckoPerson;
use App\Dto\PersonOrganizationDto;
use App\Dto\Sync\NewPersonOrganizationsDto;
use App\Dto\Sync\OrganizationDto;
use App\Dto\Sync\PersonDto;
use App\Enums\OrganizationTypeEnum;
use App\Events\NewOrganizationsForPersonsAddedEvent;
use App\Models\Organization;
use App\Models\Organization as OrganizationModel;
use App\Models\Person;
use App\Repositories\PersonOrganizationRepository;
use App\Repositories\PersonRepository;

readonly class PersonService
{
    private const SYNC_ORGANIZATION_CHUNK_SIZE = 500;

    public function __construct(
        private PersonRepository             $personRepository,
        private PersonOrganizationRepository $personOrganizationRepository,
        private CheckoClient                 $checkoClient,
    )
    {
    }

    public function syncOrganizations(): void
    {
        $persons = [];
        $newSyncId = $this->personOrganizationRepository->getLastSyncId() + 1;

        foreach ($this->personRepository->allCursor()->chunk(self::SYNC_ORGANIZATION_CHUNK_SIZE) as $chunk) {
            /** @var Person $personModel */
            foreach ($chunk as $personModel) {
                $person = $this->checkoClient->getPerson($personModel->inn);
                $newOrganizations = $this->syncOrganizationsForPerson($person, $personModel, $newSyncId);

                if (count($newOrganizations) === 0) {
                    continue;
                }

                $persons[] = new PersonDto(
                    $personModel->first_name,
                    $personModel->last_name,
                    $personModel->middle_name,
                    $personModel->inn,
                    array_map(fn(CheckoOrganization $organizationDto) => new OrganizationDto(
                        $organizationDto->inn,
                        $organizationDto->data,
                        $organizationDto->type,
                    ), $newOrganizations)
                );
            }
        }

        if (count($persons) === 0) {
            return;
        }

        NewOrganizationsForPersonsAddedEvent::dispatch(new NewPersonOrganizationsDto($persons, $newSyncId));
    }

    public function getAddedOrganization(int $syncId): NewPersonOrganizationsDto
    {
        $persons = $this->personRepository->getAddedPersonOrganizations($syncId)->map(function (Person $personModel) {

            return new PersonDto(
                $personModel->first_name,
                $personModel->last_name,
                $personModel->middle_name,
                $personModel->inn,
                array_map(function (Organization $organization) {
                    return new OrganizationDto(
                        $organization->inn,
                        $organization->data,
                        OrganizationTypeEnum::from($organization->pivot->type),
                    );
                }, $personModel->organizations->all())
            );
        })->all();

        return new NewPersonOrganizationsDto($persons, $syncId);
    }
    /**
     * @return array<int, CheckoOrganization>
     */
    private function syncOrganizationsForPerson(CheckoPerson $person, Person $personModel, int $syncId): array
    {
        $newOrganizations = [];
        $newPersonOrganizations = [];

        foreach ($person->organizations as $organization) {
            /** @var OrganizationModel $organizationModel */
            $organizationModel = OrganizationModel::query()->updateOrCreate([
                'inn' => $organization->inn,
            ], [
                'data' => $organization->data,
            ]);

            $personOrganizationDto = new PersonOrganizationDto(
                $personModel->id,
                $organizationModel->id,
                $organization->type,
                $syncId,
            );

            if (!$this->personOrganizationRepository->exists($personOrganizationDto)) {
                $newOrganizations[] = $organization;
                $newPersonOrganizations[] = $personOrganizationDto;
            }
        }

        $this->personOrganizationRepository->insert($newPersonOrganizations);

        return $newOrganizations;
    }
}
