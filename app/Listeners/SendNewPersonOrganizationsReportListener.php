<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\NewOrganizationsForPersonsAddedEvent;
use App\Mail\NewOrganizationsForPersonsAddedMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendNewPersonOrganizationsReportListener
{
    public function handle(NewOrganizationsForPersonsAddedEvent $event): void
    {
        Mail::to(config('reports.mail_for_reports'))->send(new NewOrganizationsForPersonsAddedMail(
            $event->newPersonOrganizations,
        ));

        Log::info('email sent'); //todo: test
    }
}
