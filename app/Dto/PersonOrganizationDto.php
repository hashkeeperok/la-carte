<?php

declare(strict_types=1);

namespace App\Dto;

use App\Enums\OrganizationTypeEnum;

readonly class PersonOrganizationDto
{
    public function __construct(
        public int                  $personId,
        public int                  $organizationId,
        public OrganizationTypeEnum $type,
        public int                  $syncId,
    )
    {
    }
}
