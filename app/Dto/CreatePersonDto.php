<?php

declare(strict_types=1);

namespace App\Dto;

readonly class CreatePersonDto
{
    public function __construct(
        public string $fistName,
        public string $lastName,
        public string $middleName,
        public string $inn,
    ) {
    }
}
