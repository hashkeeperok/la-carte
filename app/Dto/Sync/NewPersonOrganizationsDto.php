<?php

declare(strict_types=1);

namespace App\Dto\Sync;

readonly class NewPersonOrganizationsDto
{
    /**
     * @param array<int, PersonDto> $persons
     */
    public function __construct(
        public array $persons,
        public int $syncId,
    )
    {
    }
}
