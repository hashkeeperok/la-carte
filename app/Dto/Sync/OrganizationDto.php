<?php

declare(strict_types=1);

namespace App\Dto\Sync;

use App\Enums\OrganizationTypeEnum;

readonly class OrganizationDto
{
    public function __construct(
        public string               $inn,
        public array                $data,
        public OrganizationTypeEnum $type,
    )
    {
    }
}
