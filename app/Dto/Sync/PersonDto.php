<?php

declare(strict_types=1);

namespace App\Dto\Sync;

readonly class PersonDto
{
    /**
     * @param array<int, OrganizationDto> $organizations
     */
    public function __construct(
        public string $firstName,
        public string $lastName,
        public string $middleName,
        public string $inn,
        public array $organizations,
    )
    {
    }
}
