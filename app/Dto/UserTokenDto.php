<?php

declare(strict_types=1);

namespace App\Dto;

class UserTokenDto
{
    public function __construct(
        readonly public string $email,
        readonly public string $token,
    ) {
    }
}
