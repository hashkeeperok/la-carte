<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Services\PersonService;
use Illuminate\Console\Command;

class PersonSync extends Command
{
    protected $signature = 'person:sync';

    protected $description = 'Person Sync';

    public function handle(
        PersonService $personService,
    ): int
    {
        $personService->syncOrganizations();

        return self::SUCCESS;
    }
}
