<?php

declare(strict_types=1);

namespace App\Console\Commands\User;

use App\Dto\CreateUserDto;
use App\Repositories\UserRepository;
use Illuminate\Console\Command;

class UserCreateDefault extends Command
{
    protected $signature = 'user:create:default';

    protected $description = 'Add default user from env';

    public function handle(UserRepository $userRepository): int
    {
        $email = config('auth.default_user.email');

        if ($email === null) {
            $this->error('Email не задан');

            return self::FAILURE;
        }

        $user = $userRepository->findByEmail($email);

        if ($user !== null) {
            $this->info('Пользователь уже создан');

            return self::FAILURE;
        }

        $password = config('auth.default_user.password');

        if ($password === null) {
            $this->error('Password не задан');

            return self::FAILURE;
        }

        $userRepository->createUser(new CreateUserDto(
            $email,
            'default',
            $password,
        ));

        $this->info('Пользователь успешно создан');

        return self::SUCCESS;
    }
}
