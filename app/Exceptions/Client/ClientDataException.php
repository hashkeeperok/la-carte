<?php

declare(strict_types=1);

namespace App\Exceptions\Client;

use Throwable;

class ClientDataException extends ClientException
{
    public function __construct(string $method, string $url, ?Throwable $previous = null)
    {
        $message = sprintf('Invalid data received from %s %s', $method, $url);

        parent::__construct($message, 0, $previous);
    }
}
