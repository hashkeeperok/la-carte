<?php

declare(strict_types=1);

namespace App\Exceptions\Client;

use Throwable;

class ClientResponseCodeException extends ClientException
{
    public function __construct(string $method, string $url, int $code, ?Throwable $previous = null)
    {
        $message = sprintf('Wrong response code (%d) from %s %s', $code, $method, $url);

        parent::__construct($message, 0, $previous);
    }
}
