<?php

declare(strict_types=1);

namespace App\Exceptions\Client;

use Exception;

class ClientException extends Exception
{
}
