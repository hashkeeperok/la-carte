<?php

declare(strict_types=1);

namespace App\Enums;

enum OrganizationTypeEnum: int
{
    case Head = 0;
    case Founder = 1;
    case Individual = 2;
}
