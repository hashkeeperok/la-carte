<?php

namespace App\Providers;

use App\Clients\Checko\CheckoClient;
use GuzzleHttp\Client;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(CheckoClient::class, static fn (Application $app) => new CheckoClient(
            $app->make(Client::class),
            config('services.checko.host'),
            config('services.checko.api_key'),
        ));
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
