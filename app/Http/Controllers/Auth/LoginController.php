<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Responses\EmptySuccessResponse;
use App\Http\Responses\UserTokenResponse;
use App\Services\AuthService;
use Exception;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class LoginController extends Controller
{
    public function __construct(
        protected AuthService $authService,
    )
    {
    }

    public function login(LoginRequest $request): UserTokenResponse
    {
        try {
            $userToken = $this->authService->login($request->email, $request->password);
        } catch (Exception $exception) {
            throw new Exception('Ошибка авторизации: ' . $exception->getMessage(), 500, $exception);

            //todo Exception
        }

        return new UserTokenResponse(
            $userToken->email,
            $userToken->token,
        );
    }

    public function logout(): EmptySuccessResponse
    {
        $this->authService->logout(auth()->user());

        return new EmptySuccessResponse();
    }
}
