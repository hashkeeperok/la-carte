<?php

namespace App\Http\Controllers;

use App\Dto\CreatePersonDto;
use App\Dto\Sync\OrganizationDto;
use App\Dto\Sync\PersonDto;
use App\Dto\UpdatePersonDto;
use App\Http\Requests\CreatePersonRequest;
use App\Http\Requests\UpdatePersonRequest;
use App\Http\Responses\EmptySuccessResponse;
use App\Http\Responses\Person\AddedPersonOrganizationsResponse;
use App\Http\Responses\Person\getPersonResponse;
use App\Http\Responses\Person\PersonsResponse;
use App\Http\Responses\Person\PersonWithOrganizationsResponse;
use App\Http\Responses\Person\Sub\OrganizationResponse;
use App\Http\Responses\Person\Sub\PersonResponse;
use App\Http\Responses\Person\SyncResponse;
use App\Http\Responses\Person\SyncsResponse;
use App\Jobs\AddNewSyncJob;
use App\Models\Person;
use App\Repositories\PersonOrganizationRepository;
use App\Repositories\PersonRepository;
use App\Services\PersonService;

class PersonController extends Controller
{
    public function __construct(
        private readonly PersonRepository             $personRepository,
        private readonly PersonOrganizationRepository $personOrganizationRepository,
        private readonly PersonService                $personService,
    )
    {
    }

    public function index()
    {
        $persons = $this->personRepository->getAll();

        return new PersonsResponse(
            $persons->map(fn (Person $person) => new PersonResponse(
                $person->id,
                $person->first_name,
                $person->last_name,
                $person->middle_name,
                $person->inn,
            ))->all(),
        );
    }

    public function delete($id)
    {
        $this->personRepository->delete($id);

        return new EmptySuccessResponse();
    }

    public function create(CreatePersonRequest $request)
    {
        $this->personRepository->createPerson(new CreatePersonDto(
            $request->firstName,
            $request->lastName,
            $request->middleName,
            $request->inn,
        ));

        return new EmptySuccessResponse();
    }

    public function update(int $id, UpdatePersonRequest $request)
    {
        $this->personRepository->updatePerson($id, new UpdatePersonDto(
            $request->firstName,
            $request->lastName,
            $request->middleName,
            $request->inn,
        ));

        return new EmptySuccessResponse();
    }

    public function get($id)
    {
        $person = $this->personRepository->get($id);

        return new getPersonResponse(new PersonResponse(
            $person->id,
            $person->first_name,
            $person->last_name,
            $person->middle_name,
            $person->inn,
        ));
    }

    public function syncs()
    {
        return new SyncsResponse(
            $this->personOrganizationRepository
                ->getSyncIds()
                ->map(fn(\stdClass $row) => new SyncResponse($row->sync_id))
                ->all(),
        );
    }

    public function getSync(int $syncId)
    {
        return new AddedPersonOrganizationsResponse(
            array_map(
                fn(PersonDto $personDto) => new PersonWithOrganizationsResponse(
                    $personDto->firstName,
                    $personDto->lastName,
                    $personDto->middleName,
                    $personDto->inn,
                    array_map(
                        fn(OrganizationDto $organizationDto) => new OrganizationResponse(
                            $organizationDto->inn,
                            $organizationDto->data,
                            $organizationDto->type,
                        ),
                        $personDto->organizations,
                    ),
                ),
                $this->personService->getAddedOrganization($syncId)->persons,
            )
        );
    }

    public function newSync()
    {
        AddNewSyncJob::dispatch();

        return new EmptySuccessResponse();
    }
}
