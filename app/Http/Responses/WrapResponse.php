<?php

declare(strict_types=1);

namespace App\Http\Responses;

use Spatie\LaravelData\Support\Wrapping\Wrap;
use Spatie\LaravelData\Support\Wrapping\WrapType;

class WrapResponse extends Wrap
{
    public function __construct(
        readonly bool $success,
        readonly string $message,
        readonly array $errors = [],
    ) {
        parent::__construct(WrapType::UseGlobal);
    }

    public function wrap(?array $data = null): array
    {
        return [
            'success' => $this->success,
            'message' => $this->message,
            'data' => $data,
            'errors' => $this->errors,
        ];
    }
}
