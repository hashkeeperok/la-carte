<?php

declare(strict_types=1);

namespace App\Http\Responses;

use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class EmptySuccessResponse extends JsonResponse
{
    private const MESSAGE = 'success';

    public function __construct()
    {
        parent::__construct((new WrapResponse(true, self::MESSAGE))->wrap(), Response::HTTP_OK);
    }
}
