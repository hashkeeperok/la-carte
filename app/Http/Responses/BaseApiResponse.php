<?php

declare(strict_types=1);

namespace App\Http\Responses;

use Illuminate\Http\Request;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Support\Wrapping\Wrap;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class BaseApiResponse extends Data
{
    private const MESSAGE = 'success';

    public function getWrap(): Wrap
    {
        return new WrapResponse(true, self::MESSAGE);
    }

    protected function calculateResponseStatus(Request $request): int
    {
        return ResponseAlias::HTTP_OK;
    }
}
