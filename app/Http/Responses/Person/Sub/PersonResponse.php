<?php

declare(strict_types=1);

namespace App\Http\Responses\Person\Sub;

use Spatie\LaravelData\Data;

class PersonResponse extends Data
{
    public function __construct(
        readonly public string $id,
        readonly public string $firstName,
        readonly public string $lastName,
        readonly public string $middleName,
        readonly public string $inn,
    )
    {
    }
}
