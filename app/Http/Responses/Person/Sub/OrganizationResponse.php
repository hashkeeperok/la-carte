<?php

declare(strict_types=1);

namespace App\Http\Responses\Person\Sub;

use App\Enums\OrganizationTypeEnum;
use Spatie\LaravelData\Data;

class OrganizationResponse extends Data
{
    public function __construct(
        readonly public string               $inn,
        readonly public array                $data,
        readonly public OrganizationTypeEnum $type,
    )
    {
    }
}
