<?php

declare(strict_types=1);

namespace App\Http\Responses\Person;

use App\Http\Responses\Person\Sub\OrganizationResponse;
use Spatie\LaravelData\Data;

class PersonWithOrganizationsResponse extends Data
{
    /**
     * @param array<int, OrganizationResponse> $organizations
     */
    public function __construct(
        readonly public string $firstName,
        readonly public string $lastName,
        readonly public string $middleName,
        readonly public string $inn,
        readonly public array $organizations,
    )
    {
    }
}
