<?php

declare(strict_types=1);

namespace App\Http\Responses\Person;

use App\Http\Responses\BaseApiResponse;

class SyncsResponse extends BaseApiResponse
{
    public function __construct(
        public readonly array $syncs,
    )
    {
    }
}
