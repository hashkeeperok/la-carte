<?php

declare(strict_types=1);

namespace App\Http\Responses\Person;

use App\Http\Responses\BaseApiResponse;
use App\Http\Responses\Person\Sub\PersonResponse;

class getPersonResponse extends BaseApiResponse
{
    public function __construct(
        readonly PersonResponse $person,
    )
    {
    }
}
