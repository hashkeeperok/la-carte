<?php

declare(strict_types=1);

namespace App\Http\Responses\Person;

use App\Http\Responses\BaseApiResponse;
use App\Http\Responses\Person\Sub\PersonResponse;

class PersonsResponse extends BaseApiResponse
{
    /**
     * @param array<int, PersonResponse> $persons
     */
    public function __construct(
        readonly public array $persons,
    )
    {
    }
}
