<?php

declare(strict_types=1);

namespace App\Http\Responses\Person;

use App\Http\Responses\BaseApiResponse;

class AddedPersonOrganizationsResponse extends BaseApiResponse
{
    public function __construct(
        public array $persons,
    )
    {
    }
}
