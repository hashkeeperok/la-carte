<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Spatie\LaravelData\Attributes\Validation;
use Spatie\LaravelData\Data;

class CreatePersonRequest extends Data
{
    public function __construct(
        #[Validation\Max(255)]
        readonly public string $firstName,
        #[Validation\Max(255)]
        readonly public string $lastName,
        #[Validation\Max(255)]
        readonly public string $middleName,
        #[Validation\Max(20)]
        readonly public string $inn,
    ) {
    }
}
