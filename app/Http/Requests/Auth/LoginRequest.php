<?php

declare(strict_types=1);

namespace App\Http\Requests\Auth;

use Spatie\LaravelData\Attributes\Validation;
use Spatie\LaravelData\Data;

class LoginRequest extends Data
{
    public function __construct(
        #[Validation\Max(255)]
        #[Validation\Email]
        readonly public string $email,
        #[Validation\Min(3)]
        #[Validation\Max(20)]
        readonly public string $password,
    ) {
    }
}
