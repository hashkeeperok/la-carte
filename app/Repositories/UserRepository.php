<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Dto\CreateUserDto;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    public function findByEmail(string $email): ?User
    {
        /** @var User|null $user */
        $user = User::query()->where('email', $email)->first();

        return $user;
    }

    public function createUser(CreateUserDto $dto): void
    {
        $user = new User();

        $user->email = $dto->email;
        $user->name = $dto->name;
        $user->password = Hash::make($dto->password);

        $user->save();
    }
}
