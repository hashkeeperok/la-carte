<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Dto\CreatePersonDto;
use App\Dto\UpdatePersonDto;
use App\Models\Person;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\LazyCollection;

class PersonRepository
{
    public function getAll(): Collection
    {
        return Person::query()->orderByDesc('id')->get();
    }

    public function get($id): Person
    {
        /** @var Person|null $person */
        $person = Person::query()->where('id', $id)->first();

        if ($person === null) {
            throw new Exception('Физ лицо не найдено', 404);
        }

        return $person;
    }

    public function delete(int $id): mixed
    {
        /** @var Person|null $person */
        $person = Person::query()->where('id', $id)->first();

        if ($person === null) {
            throw new Exception('Физ лицо не найдено', 404);
        }

        return Person::query()->where('id', $id)->delete();
    }

    public function createPerson(CreatePersonDto $dto): void
    {
        $person = new Person();

        $person->first_name = $dto->fistName;
        $person->last_name = $dto->lastName;
        $person->middle_name = $dto->middleName;
        $person->inn = $dto->inn;

        $person->save();
    }

    public function updatePerson(int $id, UpdatePersonDto $dto): void
    {
        /** @var Person|null $person */
        $person = Person::query()->where('id', $id)->first();

        if ($person === null) {
            throw new Exception('Физ лицо не найдено', 404);
        }

        $person->first_name = $dto->fistName;
        $person->last_name = $dto->lastName;
        $person->middle_name = $dto->middleName;
        $person->inn = $dto->inn;

        $person->save();
    }

    public function getById(int $id): Person
    {
        /** @var Person|null $person */
        $person = Person::query()->where('id', $id)->first();

        if ($person === null) {
            throw new Exception('Физ лицо не найдено', 404);
        }

        return $person;
    }

    public function allCursor(): LazyCollection
    {
        return Person::query()->orderBy('id')->cursor();
    }

    public function getAddedPersonOrganizations(int $syncId): Collection
    {
        return Person::query()
            ->whereHas('organizations', fn (Builder $query) => $query->where('sync_id', $syncId))
            ->with('organizations')
            ->get();
    }
}
