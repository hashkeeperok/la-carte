<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Dto\PersonOrganizationDto;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PersonOrganizationRepository
{
    /**
     * @param array<int, PersonOrganizationDto> $data
     */
    public function insert(array $data): bool
    {
        $insertData = array_map(fn(PersonOrganizationDto $dto) => [
            'person_id' => $dto->personId,
            'organization_id' => $dto->organizationId,
            'type' => $dto->type->value,
            'sync_id' => $dto->syncId,
        ], $data);

        return DB::table('organization_person')->insert($insertData);
    }

    public function exists(PersonOrganizationDto $dto): bool
    {
        return DB::table('organization_person')
            ->where('person_id', $dto->personId)
            ->where('organization_id', $dto->organizationId)
            ->where('type', $dto->type->value)
            ->exists();
    }

    public function getLastSyncId(): int
    {
        return (int) DB::table('organization_person')
            ->select('sync_id')
            ->orderByDesc('id')
            ->first()
            ?->sync_id;
    }

    public function getSyncIds(): Collection
    {
        return DB::table('organization_person')
            ->select('sync_id')
            ->distinct()
            ->orderByDesc('sync_id')
            ->get();
    }
}
