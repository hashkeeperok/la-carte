<?php

declare(strict_types=1);

namespace App\Mail;

use App\Dto\Sync\NewPersonOrganizationsDto;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class NewOrganizationsForPersonsAddedMail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public NewPersonOrganizationsDto $newPersonOrganizationsDto
    )
    {
    }

    public function envelope(): Envelope
    {
        return new Envelope(
            from: new Address('send@alacarte-dev.1gb.ru', 'La Carte - DEV'),
            subject: 'Новые организации физ. лиц',
        );
    }

    public function content(): Content
    {
        return new Content(
            view: 'mail.new_organizations_for_persons_added',
        );
    }
}
