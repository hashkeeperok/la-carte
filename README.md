# La Carte - Test

## Локальный сервер

Для развертывания проекта на локальной машине и запуска локального сервера выполните следующие команды.

```Bash
git clone git@github.com:hashkeeperok/la-carte-test.git
```
```Bash
cd la-carte-test/
```
```Bash
docker run --rm -u "$(id -u):$(id -g)" -v "$(pwd):/var/www/html" -w /var/www/html laravelsail/php83-composer:latest composer install --ignore-platform-reqs
```
```Bash
cp .env.example .env
```
```Bash
docker-compose build
```
```Bash
docker-compose up -d
```
