<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', [\App\Http\Controllers\Auth\LoginController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::post('logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout']);
});

Route::middleware('auth:sanctum')->group(function () {
    Route::get('persons/syncs', [\App\Http\Controllers\PersonController::class, 'syncs']);
    Route::get('persons/syncs/{syncId}', [\App\Http\Controllers\PersonController::class, 'getSync']);
    Route::post('persons/syncs', [\App\Http\Controllers\PersonController::class, 'newSync']);

    Route::get('persons', [\App\Http\Controllers\PersonController::class, 'index']);
    Route::delete('persons/{id}', [\App\Http\Controllers\PersonController::class, 'delete']);
    Route::get('persons/{id}', [\App\Http\Controllers\PersonController::class, 'get']);
    Route::put('persons/{id}', [\App\Http\Controllers\PersonController::class, 'update']);
    Route::post('persons', [\App\Http\Controllers\PersonController::class, 'create']);
});
